<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.util.ArrayList" %>
<%@ page import="inapp.demo.classes.Student" %>

 
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
<h1>Student Details</h1>
<div>
</div>
 <form action="DemoServlet" method="get"> 
  <table>
  	<thead>
  	<tr>
  	<th>ID</th>
  	<th>NAME</th>
  	</tr>
  	</thead>
  	<tbody>
  
  	<% ArrayList<Student> record = (ArrayList<Student>) request.getAttribute("studentRec"); 
  	for(Student stud:record){ %>   
    	<tr>
    		<td>
    			<%= stud.getId() %>
    		</td>
    		<td>
    			<%= stud.getName() %>
    		</td>
    	</tr>
    	<% } %> 
    	</tbody>
    </table>
        
    </form> 
    
</body>
</html>