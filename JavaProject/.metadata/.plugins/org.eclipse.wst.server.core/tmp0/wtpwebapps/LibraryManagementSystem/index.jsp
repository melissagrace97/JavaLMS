<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login</title>
<link rel="stylesheet" href="css/login.css">
<script type="text/javascript">
	function validate() {
		var name = document.getElementById("email").value;
		var pass = document.getElementById("pass").value;

		if (name == "") {
			document.getElementById("emailerror").innerHTML = "Please enter Email";
			return false;
		} else {
			document.getElementById("emailerror").innerHTML = "";
		}
		if (pass == "") {
			document.getElementById("passerror").innerHTML = "Please enter Password";
			return false;
		} else {
			document.getElementById("passerror").innerHTML = "";
		}
		if (pass.length<5 || pass.length>8) {
			document.getElementById("passerror").innerHTML = "Password should be in between 5 to 8 characters";
		} else {
			document.getElementById("passerror").innerHTML = "";
		}
		return true;
	}
</script>
</head>
<body>

	<form onsubmit="return validate()" action="LoginServlet" method="post">
		<div class="container stack-top">
			<h2>LIBRARY MANAGEMENT SYSTEM</h2>

			<div class="card">
				<div class="lms_image">
					<div class="lms_cover"></div>
				</div>
				<div class="login_box">
					<h1>Login</h1>
					<form>
						<p>Username</p>
						<input type="email" placeholder="Username" name="email" id="email"
							required> <span style="color: red" id="emailerror">*</span>
						<p>Password</p>
						<input type="password" placeholder="Password" name="password"
							id="pass" required> <span style="color: red"
							id="passerror">*</span> <input type="submit" value="Login">
						<a href="registeration.jsp">New User? Register</a>

						<p class="status">
							<%
								String status = (String) request.getAttribute("status");
							if (status != null) {
								out.println(status);
							}
							%>
						
						<p>
					</form>
				</div>

			</div>

		</div>
	</form>
</body>
</html>