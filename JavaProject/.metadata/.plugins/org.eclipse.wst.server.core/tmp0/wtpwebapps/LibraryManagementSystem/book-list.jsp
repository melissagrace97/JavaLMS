<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Book</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
</head>
<body>
	<header>
		<nav class="navbar navbar-expand-md navbar-dark"
			style="background-color: tomato">
			<div>
				<a href="" class="navbar-brand">LIBRARY MANAGEMENT SYSTEM</a>
			</div>
			<ul class="navbar-nav">
				<li><a href="<%=request.getContextPath()%>/list"
					class="nav-link">Books</a></li>
			</ul>
		</nav>
	</header>
	<br>

	<div class="row">
		<div class="container">
			<h3 class="text-center">List Of Books</h3>
			<hr>
			<div class="container text-left">
				<a href="<%=request.getContextPath()%>/new" class="btn btn-success">Add
					New Book</a>

			</div>
			<br>
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>Book ID</th>
						<th>Book Name</th>
						<th>Author Name</th>
						<th>Category</th>
						<th>Published Year</th>
						<th>Available Copies</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="book" items="${listBook }">
						<tr>
							<td><c:out value="${book.book_id }" /></td>
							<td><c:out value="${book.book_name }" /></td>
							<td><c:out value="${book.author_name }" /></td>
							<td><c:out value="${book.category }" /></td>
							<td><c:out value="${book.published_year }" /></td>
							<td><c:out value="${book.available_copies }" /></td>
							<td><a
								hred="edit?book_id=<c:out value='${book.book_id }' />">Edit</a>
								&nbsp;&nbsp;&nbsp;&nbsp <a
								hred="delete?book_id=<c:out value='${book.book_id }' />">Delete</a></td>
						</tr>
					</c:forEach>

				</tbody>
			</table>
		</div>
	</div>
</body>
</html>