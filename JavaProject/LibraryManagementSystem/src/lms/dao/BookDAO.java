package lms.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import lms.classes.Books;

//CRUD operations for book table in the database
public class BookDAO {
	private String jdbcURL = "jdbc:mysql://localhost:3306/librarydb";
	private String jdbcUsername = "root";
	private String jdbcPassword = "1234";
	private static final String INSERT_BOOKS_SQL = "insert into books(book_name,author_name,category,published_year,available_copies) values(?,?,?,?,?)";
	private static final String SELECT_ALL_BOOKS = "select * from books";
	private static final String SELECT_BOOKS_BY_ID = "select book_id, book_name, author_name, category, published_year, available_copies from books where book_id = ?";
	private static final String DELETE_BOOKS_SQL = "delete from books where book_id=?";
	private static final String UPDATE_BOOKS_SQL = "update books set book_name = ?, author_name=?, category=?, published_year=?, available_copies=? where book_id=?";
	private static final String SELECT_BOOKS_BY_NAME_OR_CATEGORY = "select * from books where book_name like '%\"+query+\"%' or category like '%\"+query+\"%'";
	protected Connection getConnection() {
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return connection;
	}
	//create or insert books
	public void insertBook(Books book) throws SQLException{
		try(Connection connection = getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(INSERT_BOOKS_SQL)){
			preparedStatement.setString(1, book.getBook_name());
			preparedStatement.setString(2, book.getAuthor_name());
			preparedStatement.setString(3, book.getCategory());
			preparedStatement.setString(4, book.getPublished_year());
			preparedStatement.setInt(5, book.getAvailable_copies());
			preparedStatement.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	//update book
	public boolean updateBook(Books book) throws SQLException{
		boolean rowUpdated;
		try(Connection connection = getConnection();
				PreparedStatement statement = connection.prepareStatement(UPDATE_BOOKS_SQL)){
			statement.setString(1, book.getBook_name());
			statement.setString(2, book.getAuthor_name());
			statement.setString(3, book.getCategory());
			statement.setString(4, book.getPublished_year());
			statement.setInt(5, book.getAvailable_copies());
			statement.setInt(6, book.getBook_id());
			rowUpdated = statement.executeUpdate() > 0;
		}
		return rowUpdated;
	}
	//select book by id
	public Books selectBook(int book_id) {
		Books book = null;
		
		try(Connection connection = getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SELECT_BOOKS_BY_ID)){
			preparedStatement.setInt(1, book_id);
			ResultSet rs = preparedStatement.executeQuery();
			while(rs.next()) {
				String book_name = rs.getString("book_name");
				String author_name = rs.getString("author_name");
				String category = rs.getString("category");
				String published_year = rs.getString("published_year");
				int available_copies = rs.getInt("available_copies");
				book = new Books(book_id, book_name, author_name, category, published_year, available_copies);
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return book;
	}
	//select books
	public List<Books> selectAllBooks() {
		List<Books> book = new ArrayList<>();
		
		try(Connection connection = getConnection();
				PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_BOOKS)){
			System.out.println(preparedStatement);
			ResultSet rs = preparedStatement.executeQuery();
			while(rs.next()) {
				int book_id = rs.getInt("book_id");
				String book_name = rs.getString("book_name");
				String author_name = rs.getString("author_name");
				String category = rs.getString("category");
				String published_year = rs.getString("published_year");
				int available_copies = rs.getInt("available_copies");
				book.add(new Books(book_id, book_name, author_name, category, published_year, available_copies));
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return book;
	}
	//delete user
	public boolean deleteBooks(int book_id) throws SQLException{
		boolean rowDeleted;
		try(Connection connection = getConnection();
				PreparedStatement statement = connection.prepareStatement(DELETE_BOOKS_SQL);){
			statement.setInt(1, book_id);
			rowDeleted = statement.executeUpdate() > 0;
			}
		return rowDeleted;
	}
}
