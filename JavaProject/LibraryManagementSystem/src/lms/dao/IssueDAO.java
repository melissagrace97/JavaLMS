package lms.dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.sql.Date;
import java.util.List;
import lms.classes.IssueBook;

public class IssueDAO {

	private String jdbcURL = "jdbc:mysql://localhost:3306/librarydb";
	private String jdbcUsername = "root";
	private String jdbcPassword = "1234";
	private static final String SELECT_ALL_ISSUE_BOOKS = "select * from issuebook";
	private static final String INSERT_INTO_ISSUE_BOOKS = "insert into issuebook(id,book_id,name,book_name,issue_date,due_date) values(?,?,?,?,?,?)";
	protected Connection getConnection() {
		Connection connection = null;
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
			connection = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
		}catch(Exception e) {
			e.printStackTrace();
		}
		return connection;
	}
	
	//insert issue records
	public void insertIssueBook(IssueBook issue) throws SQLException{
		try(Connection connection = getConnection();
				PreparedStatement pt = connection.prepareStatement(INSERT_INTO_ISSUE_BOOKS)){
			pt.setInt(1, issue.getId());
			pt.setInt(2, issue.getBook_id());
			pt.setString(3, issue.getName());
			pt.setString(4, issue.getBook_name());
			pt.setDate(5, issue.getIssue_date());
			pt.setDate(6, issue.getDue_date());
			pt.executeUpdate();
		}catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	//select issue records
		public List<IssueBook> selectAllIssueBooks() {
			List<IssueBook> issue = new ArrayList<>();	
			try(Connection connection = getConnection();
					PreparedStatement preparedStatement = connection.prepareStatement(SELECT_ALL_ISSUE_BOOKS)){
				System.out.println(preparedStatement);
				ResultSet rs = preparedStatement.executeQuery();
				while(rs.next()) {	
					int id = rs.getInt("id");
					int book_id = rs.getInt("book_id");
					String name = rs.getString("name");
					String book_name = rs.getString("book_name");
					Date issue_date = rs.getDate("issue_date");
					Date due_date = rs.getDate("due_date");
					issue.add(new IssueBook(id, book_id, name, book_name, issue_date, due_date));
				}
			}catch(Exception e) {
				e.printStackTrace();
			}
			return issue;
		}
}
