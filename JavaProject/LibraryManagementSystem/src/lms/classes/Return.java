package lms.classes;

public class Return {

	private int id;
	private String name;
	private int book_id;
	private String book_name;
	private String issue_date;
	private String return_date;
	private float fine;
	
	
	public Return(int id, String name, int book_id, String book_name, String issue_date, String return_date,
			float fine) {
		super();
		this.id = id;
		this.name = name;
		this.book_id = book_id;
		this.book_name = book_name;
		this.issue_date = issue_date;
		this.return_date = return_date;
		this.fine = fine;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getBook_id() {
		return book_id;
	}
	public void setBook_id(int book_id) {
		this.book_id = book_id;
	}
	public String getBook_name() {
		return book_name;
	}
	public void setBook_name(String book_name) {
		this.book_name = book_name;
	}
	public String getIssue_date() {
		return issue_date;
	}
	public void setIssue_date(String issue_date) {
		this.issue_date = issue_date;
	}
	public String getReturn_date() {
		return return_date;
	}
	public void setReturn_date(String return_date) {
		this.return_date = return_date;
	}
	public float getFine() {
		return fine;
	}
	public void setFine(float fine) {
		this.fine = fine;
	}
	
	
	
}
