package lms.classes;

public class Books {

	private int book_id;
	private String book_name;
	private String author_name;
	private String category;
	private String published_year;
	private int available_copies;
	
	public Books(String book_name, String author_name, String category, String published_year, int available_copies) {
		super();
		this.book_name = book_name;
		this.author_name = author_name;
		this.category = category;
		this.published_year = published_year;
		this.available_copies = available_copies;
	}
	public Books(int book_id, String book_name, String author_name, String category, String published_year,
			int available_copies) {
		super();
		this.book_id = book_id;
		this.book_name = book_name;
		this.author_name = author_name;
		this.category = category;
		this.published_year = published_year;
		this.available_copies = available_copies;
	}
	public int getBook_id() {
		return book_id;
	}
	public void setBook_id(int book_id) {
		this.book_id = book_id;
	}
	public String getBook_name() {
		return book_name;
	}
	public void setBook_name(String book_name) {
		this.book_name = book_name;
	}
	public String getAuthor_name() {
		return author_name;
	}
	public void setAuthor_name(String author_name) {
		this.author_name = author_name;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public String getPublished_year() {
		return published_year;
	}
	public void setPublished_year(String published_year) {
		this.published_year = published_year;
	}
	public int getAvailable_copies() {
		return available_copies;
	}
	public void setAvailable_copies(int available_copies) {
		this.available_copies = available_copies;
	}
}
