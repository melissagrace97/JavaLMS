package lms.classes;

import java.sql.Date;

public class IssueBook {
	private int id;
	private int book_id;
	private String name;
	private String book_name;
	private Date issue_date;
	private Date due_date;
	
	public IssueBook(int id, int book_id, String name, String book_name, Date issue_date, Date due_date) {
		super();
		this.id = id;
		this.book_id = book_id;
		this.name = name;
		this.book_name = book_name;
		this.issue_date = issue_date;
		this.due_date = due_date;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public int getBook_id() {
		return book_id;
	}
	public void setBook_id(int book_id) {
		this.book_id = book_id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBook_name() {
		return book_name;
	}
	public void setBook_name(String book_name) {
		this.book_name = book_name;
	}
	public Date getIssue_date() {
		return issue_date;
	}
	public void setIssue_date(Date issue_date) {
		this.issue_date = issue_date;
	}
	public Date getDue_date() {
		return due_date;
	}
	public void setDue_date(Date due_date) {
		this.due_date = due_date;
	}
}
