package lms.servlets;

import java.io.IOException;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import lms.classes.IssueBook;
import lms.dao.IssueDAO;

@WebServlet("/IssueServlet")
public class IssueServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	private IssueDAO issueDAO;
    public IssueServlet() {
        super();
       
    }
    public void init() {
		issueDAO = new IssueDAO();
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getServletPath();
		switch (action) {
		case "/insert":
			try {
				insertIssueBooks(request, response);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			break;
		default:
			// handle list
			try {
				listIssueBook(request, response);
			} catch (SQLException e) {
				e.printStackTrace();
			}
			break;
		}
	}
	private void listIssueBook(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException{
		List<IssueBook> issueBook = issueDAO.selectAllIssueBooks();
		request.setAttribute("issueBook", issueBook);
		RequestDispatcher dispatcher = request.getRequestDispatcher("issue_books.jsp");
		dispatcher.forward(request, response);
	}
	private void insertIssueBooks(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException{
		int id = Integer.parseInt(request.getParameter("userid"));
		int bookid = Integer.parseInt(request.getParameter("bookid"));
		String name = request.getParameter("name");
		String book_name = request.getParameter("book_name");
		Date issue_date = Date.valueOf(request.getParameter("issue_date"));
		Date due_date = Date.valueOf(request.getParameter("due_date"));
		IssueBook issuebook = new IssueBook(id,bookid,name,book_name,issue_date,due_date);
		issueDAO.insertIssueBook(issuebook);
		response.sendRedirect("issue_books.jsp");
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
	}

}
