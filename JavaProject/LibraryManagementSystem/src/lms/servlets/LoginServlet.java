package lms.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpSession;
//
//import lms.classes.ConnectionPro;
//import lms.classes.User;
//import lms.classes.UserDatabase;
import javax.servlet.http.HttpSession;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	public LoginServlet() {
		super();
	}
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		if (request.getParameter("btn_login") != null) {
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			String role = request.getParameter("role");
			PreparedStatement stmt = null;
			ResultSet rs;
			Connection con;
			RequestDispatcher rd;
			int count = 0;
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				con = DriverManager.getConnection("jdbc:mysql://localhost:3306/librarydb", "root", "1234");
				System.out.println("Connection Established");
				String sql = "select * from user where email=? and password=? and role=?";
				stmt = con.prepareStatement(sql);
				stmt.setString(1, email);
				stmt.setString(2, password);
				stmt.setString(3, role);
				rs = stmt.executeQuery();
				if (rs.next()) {
					String lemail = rs.getString("email");
					String lpass = rs.getString("password");
					String lrole = rs.getString("role");
					System.out.println(lemail);
					System.out.println(lpass);
					System.out.println(lrole);
					if (email.equals(lemail) && password.equals(lpass) && role.equals(lrole)) {
						if (lrole.equals("admin")) {
							request.setAttribute("status", "Login Successful");
							rd = request.getRequestDispatcher("admin_home.jsp");
							rd.forward(request, response);	
						} else if (lrole.equals("user")) {
							request.setAttribute("status", "Login Successful");
							rd = request.getRequestDispatcher("user_home.jsp");
							rd.forward(request, response);
						}
					}
				} else {
					request.setAttribute("status", "Invalid credentials!!... Please try again");
							rd = request.getRequestDispatcher("index.jsp");
							rd.forward(request, response);
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
