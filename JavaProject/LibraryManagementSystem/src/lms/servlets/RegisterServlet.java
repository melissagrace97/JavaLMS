package lms.servlets;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import lms.classes.User;

@WebServlet("/RegisterServlet")
public class RegisterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public RegisterServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		if (request.getParameter("btn_register") != null) {
			String name = request.getParameter("name");
			String email = request.getParameter("email");
			String password = request.getParameter("password");
			String role = request.getParameter("role");
			
			PreparedStatement stmt = null;
			ResultSet rs;
			Connection con;
			RequestDispatcher rd;
			try {
				Class.forName("com.mysql.cj.jdbc.Driver");
				con = DriverManager.getConnection("jdbc:mysql://localhost:3306/librarydb", "root", "1234");
				System.out.println("Register connection established");
				stmt = con.prepareStatement("select * from user where email =?");
				stmt.setString(1, email);
				rs=stmt.executeQuery();
				if(rs.next()) {
					String checkEmail = rs.getString("email");
					if(email.equals(checkEmail)) {
						request.setAttribute("status", "Email already exists");
						rd = request.getRequestDispatcher("registeration.jsp");
						rd.forward(request, response);
					}
				}
				else {
					String sql = "insert into user(name,email,password,role) values(?,?,?,?)";
					stmt = con.prepareStatement(sql);
					stmt.setString(1, name);
					stmt.setString(2, email);
					stmt.setString(3, password);
					stmt.setString(4, role);
					int row = stmt.executeUpdate();
					if (row > 0) {
						request.setAttribute("status", "Successfully Registered. Now you can login");
						rd = request.getRequestDispatcher("index.jsp");
						rd.forward(request, response);
					} else {
						request.setAttribute("status", "Failed to Register...Try Again");
						rd = request.getRequestDispatcher("registeration.jsp");
						rd.forward(request, response);
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

	}
}
