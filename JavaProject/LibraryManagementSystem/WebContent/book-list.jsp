<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.io.PrintWriter"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Book</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

</head>
<body>
	<header>
		<nav class="navbar navbar-expand-md navbar-dark"
			style="background-color: black">
			<div>
				<a href="" class="navbar-brand">LIBRARY MANAGEMENT SYSTEM</a>
			</div>
			<ul class="navbar-nav">
				<li><a href="admin_home.jsp" class="nav-link">Home</a></li>
				<li><a href="<%=request.getContextPath()%>/list"
					class="nav-link"><strong>Books</strong></a></li>
					<li><a href="search-book.jsp" class="nav-link">Search
						Books</a></li>
				<li><a href="issuebookrequest.jsp"
					class="nav-link">Issue Book Request</a></li>
					<li><a href="issue_books.jsp" class="nav-link">Issued Books</a></li>
				<li><a href="return_books.jsp" class="nav-link">Return
						Books</a></li>
				<li><a href="logout.jsp" class="nav-link">Logout</a></li>
			</ul>
		</nav>
	</header>
	<br>
	<div class="row">
		<div class="container">
			<h3 class="text-center">List Of Books</h3>
			<br>
			<div class="row">
					<div class="col-md-4 text-left">
					</div>
					<div class="col-md-8 text-right">
						<a href="<%=request.getContextPath()%>/new"
							class="btn btn-primary">Add New Book</a>
					</div>
			</div>
			<br>
			<table class="table table-bordered table-hover">
				<thead style="background-color: rgb(100, 168, 168); color: white">
					<tr class="text-center">
						<th>Book ID</th>
						<th>Book Name</th>
						<th>Author Name</th>
						<th>Category</th>
						<th>Published Year</th>
						<th>Available Copies</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="book" items="${listBook}">
						<tr class="text-center">
							<td><c:out value="${book.book_id }" /></td>
							<td><c:out value="${book.book_name }" /></td>
							<td><c:out value="${book.author_name }" /></td>
							<td><c:out value="${book.category }" /></td>
							<td><c:out value="${book.published_year }" /></td>
							<td><c:out value="${book.available_copies }" /></td>
							<td><a
								href="edit?book_id=<c:out value='${book.book_id }' />"
								class="btn btn-warning">Edit</a> &nbsp;&nbsp;&nbsp;&nbsp; <a
								href="delete?book_id=<c:out value='${book.book_id }' />"
								class="btn btn-danger">Delete</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
	</div>
</body>
</html>