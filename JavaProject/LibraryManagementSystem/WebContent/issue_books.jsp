<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.io.PrintWriter"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Issue Books</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
</head>
<body>
<header>
		<nav class="navbar navbar-expand-md navbar-dark"
			style="background-color: black">
			<div>
				<a href="" class="navbar-brand">LIBRARY MANAGEMENT SYSTEM</a>
			</div>
			<ul class="navbar-nav">
			<li><a href="admin_home.jsp" class="nav-link">Home</a></li>
				<li><a href="<%=request.getContextPath()%>/list"
					class="nav-link">Books</a></li>
					<li><a href="search-book.jsp" class="nav-link">Search
						Books</a></li>
				<li><a href="issuebookrequest.jsp" class="nav-link">Issue Book Request</a></li>
				<li><a href="issue_books.jsp" class="nav-link"><strong>Issued Books</strong></a></li>
				<li><a href="return_books.jsp" class="nav-link">Return Books</a></li>
				<li><a href="logout.jsp" class="nav-link" >Logout</a></li>
			</ul>
		</nav>
	</header>
	<br>
	<div class="row">
		<div class="container">
			<h3 class="text-center">Issued Books</h3>
		
		<br>
		<table class="table table-bordered table-hover">
				<thead style="background-color:rgb(100, 168, 168); color: white">
					<tr class="text-center">
						<th>Reader ID</th>
						<th>Reader Name</th>
						<th>Book ID</th>
						<th>Book Name</th>
						<th>Issue Date</th>
						<th>Due Date</th>
					</tr>
				</thead>
				<tbody>

					<c:forEach var="issue" items="${listIssueBook}">
						<tr class="text-center">
							<td><c:out value="${issue.id }" /></td>
							<td><c:out value="${issue.name }" /></td>
							<td><c:out value="${issue.book_id }" /></td>
							<td><c:out value="${issue.book_name }" /></td>
							<td><c:out value="${issue.issue_date }" /></td>
							<td><c:out value="${issue.due_date }" /></td>
						</tr>
					</c:forEach>

				</tbody>
			</table>
			</div>
	</div>
</body>
</html>