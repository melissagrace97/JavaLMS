<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix ="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Books</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
</head>
<body>
	<header>
		<nav class="navbar navbar-expand-md navbar-dark"
			style="background-color: black">
			<div>
				<a href="" class="navbar-brand">LIBRARY MANAGEMENT SYSTEM</a>
			</div>
			<ul class="navbar-nav">
				<li><a href="<%=request.getContextPath()%>/list"
					class="nav-link">Books</a></li>
			</ul>
		</nav>
	</header>
	<br>
	<div class="container col-md-5">
		<div class="card">
			<div class="card-body">
				<c:if test="${book != null }">
					<form action="update" method="post">
				</c:if>
				<c:if test="${book == null }">
					<form action="insert" method="post">
				</c:if>
				<caption>
					<h2>
						<c:if test="${book != null }">
					Edit Book
				</c:if>
						<c:if test="${book == null }">
					Add New Book
				</c:if>
					</h2>
				</caption>

				<c:if test="${book != null }">
					<input type="hidden" name="book_id"
						value="<c:out value='${book.book_id }'/>" />
				</c:if>
				<fieldset class="form-group">
					<label>Book Name</label><input type="text"
						value="<c:out value='${book.book_name }' />" class="form-control"
						name="book_name" required="required">
				</fieldset>
				<fieldset class="form-group">
					<label>Author Name</label><input type="text"
						value="<c:out value='${book.author_name }' />"
						class="form-control" name="author_name" required="required">
				</fieldset>
				<fieldset class="form-group">
					<label>Category</label><input type="text"
						value="<c:out value='${book.category }' />" class="form-control"
						name="category" required="required">
				</fieldset>
				<fieldset class="form-group">
					<label>Published Year</label><input type="text"
						value="<c:out value='${book.published_year}' />"
						class="form-control" name="published_year" required="required">
				</fieldset>
				<fieldset class="form-group">
					<label>Available Copies</label><input type="text"
						value="<c:out value='${book.available_copies}' />"
						class="form-control" name="available_copies" required="required">
				</fieldset>
				<button type="submit" class="btn btn-success">Save</button>
				</form>
				<br>
			</div>
		</div>
	</div>

</body>
</html>