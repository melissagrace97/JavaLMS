<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page import="java.sql.ResultSet"%>
<%@page import="java.sql.DriverManager"%>
<%@page import="java.sql.Statement"%>
<%@page import="java.sql.Connection"%>
<%@page import="java.io.PrintWriter"%>
<%@page import="java.util.ArrayList" %>
<%@page import="java.util.Iterator" %>
<%@page import="java.sql.PreparedStatement"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Book</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">

</head>
<body>
	<header>
		<nav class="navbar navbar-expand-md navbar-dark"
			style="background-color: black">
			<div>
				<a href="" class="navbar-brand">LIBRARY MANAGEMENT SYSTEM</a>
			</div>
			<ul class="navbar-nav">
			<li><a href="user_home.jsp" class="nav-link">Home</a></li>
				<li><a href="user_view_search_books.jsp" class="nav-link"><strong>Books</strong></a></li>
				<li><a href="user_issue_book_form.jsp" class="nav-link">Book Request</a></li>
				<li><a href="" class="nav-link">My Books</a></li>
				<li><a href="" class="nav-link">Book Renewal</a></li>
				<li><a href="" class="nav-link">Issue Information</a></li>
				<li><a href="logout.jsp" class="nav-link" >Logout</a></li>
			</ul>
		</nav>
	</header>
	<br>
	<div class="row">
		<div class="container">
			<h3 class="text-center">List of Books</h3>
			<br>
				<form id="form" method="get" action="user_view_search_books.jsp">
					<div class="col-md-6">
						<input type="text" name="query" class="form-control"
							id="query" placeholder="Search by Book Name or Category">
							<button type="submit" class="btn btn-success mt-1">Search</button>
					</div>
				</form>
		<br>
			<div>
				<table class="table table-bordered table-hover">
					<thead style="background-color: rgb(100, 168, 168); color: white">
						<tr class="text-center">
							<th>Book ID</th>
							<th>Book Name</th>
							<th>Author Name</th>
							<th>Category</th>
							<th>Published Year</th>
							<th>Available Copies</th>
						</tr>
					</thead>
					<tbody>
						<%
							Connection con;
						PreparedStatement pt;
						ResultSet rs;
						String jdbcURL = "jdbc:mysql://localhost:3306/librarydb";
						String jdbcUsername = "root";
						String jdbcPassword = "1234";
						try {
							Class.forName("com.mysql.cj.jdbc.Driver");
							con = DriverManager.getConnection(jdbcURL, jdbcUsername, jdbcPassword);
							String query = request.getParameter("query");
							if (query == null || query.isEmpty()) {
								pt = con.prepareStatement("select * from books");
								rs = pt.executeQuery();
								while (rs.next()) {
						%>
						<tr>
							<td><%=rs.getString("book_id")%></td>
							<td><%=rs.getString("book_name")%></td>
							<td><%=rs.getString("author_name")%></td>
							<td><%=rs.getString("category")%></td>
							<td><%=rs.getString("published_year")%></td>
							<td><%=rs.getString("available_copies")%></td>
						</tr>
						<%
							}
						} 
							else if(query!=null) {
							String sql = "select * from books where book_name like '%"+query+"%' or category like '%"+query+"%'";
							pt = con.prepareStatement(sql);
							//pt.setString(1, bookname);
							rs = pt.executeQuery();
							while (rs.next()) {
						%>
						<tr>
							<td><%=rs.getString("book_id")%></td>
							<td><%=rs.getString("book_name")%></td>
							<td><%=rs.getString("author_name")%></td>
							<td><%=rs.getString("category")%></td>
							<td><%=rs.getString("published_year")%></td>
							<td><%=rs.getString("available_copies")%></td>
						</tr>
						<%
							}
						}
							else{
								%>
								<td>
								 out.print("No record found!");</td>
								 <%
							}
						} catch (Exception e) {
						e.printStackTrace();
						}
						%>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</body>
</html>