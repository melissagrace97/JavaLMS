<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
</head>
<body>
<header>
		<nav class="navbar navbar-expand-md navbar-dark"
			style="background-color: black">
			<div>
				<a href="" class="navbar-brand">LIBRARY MANAGEMENT SYSTEM</a>
			</div>
			<ul class="navbar-nav">
			<li><a href="user_home.jsp" class="nav-link"><strong>Home</strong></a></li>
				<li><a href="user_view_search_books.jsp" class="nav-link">Books</a></li>
				<li><a href="user_issue_book_form.jsp" class="nav-link">Book Request</a></li>
				<li><a href="" class="nav-link">My Books</a></li>
				<li><a href="" class="nav-link">Book Renewal</a></li>
				<li><a href="" class="nav-link">Issue Information</a></li>
				<li><a href="logout.jsp" class="nav-link" >Logout</a></li>
			</ul>
		</nav>
	</header>
<br>
	<h1 class="text-center">Welcome</h1>
</body>
</html>