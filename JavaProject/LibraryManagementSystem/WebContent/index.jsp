<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login</title>
<link rel="stylesheet" href="css/login.css" type="text/css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<script type="text/javascript">
	function validate() {
		var name = document.getElementById("email").value;
		var pass = document.getElementById("pass").value;

		if (name == "") {
			document.getElementById("emailerror").innerHTML = "Please enter Email";
			return false;
		} else {
			document.getElementById("emailerror").innerHTML = "";
		}
		if (pass == "") {
			document.getElementById("passerror").innerHTML = "Please enter Password";
			return false;
		} else {
			document.getElementById("passerror").innerHTML = "";
		}
		if (pass.length<5 || pass.length>8) {
			document.getElementById("passerror").innerHTML = "Password should be in between 5 to 8 characters";
		} else {
			document.getElementById("passerror").innerHTML = "";
		}
		return true;
	}
</script>
</head>
<body>
	<header>
		<nav class="navbar navbar-expand-md navbar-dark"
			style="background-color: black">
			<div>
				<a href="" class="navbar-brand">LIBRARY MANAGEMENT SYSTEM</a>
			</div>
		</nav>
	</header>
	<br>
	<div class="row">
		<div class="container">
			<h3 class="text-center">Login</h3>
			<br>
			<p class="status" style="color:red; align:center">
						<%
							String status = (String) request.getAttribute("status");
						if (status != null) {
							out.println(status);
						}
						%>
					<p>
			<div class="container">
				<form onsubmit="return validate()" action="LoginServlet"
					method="post" class="form-control">
					<div class="form-group">
						<label class="col-sm-3 control-label">Email</label>
						<div class="col-sm-6">
							<input type="email" placeholder="Username" class="form-control"
								name="email" id="email" required> <span
								style="color: red" id="emailerror">*</span>

						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Password</label>
						<div class="col-md-6">
							<input type="password" placeholder="Password" name="password"
								id="pass" required class="form-control"> <span
								style="color: red" id="passerror">*</span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Select Type</label>
						<div class="col-md-6">
							<select name="role" class="form-control">
								<option value="" selected="selected">- select role -</option>
								<option value="admin">Admin</option>
								<option value="user">User</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-offset-3 col-md-9 m-t-15">
							<input type="submit" name="btn_login" class="btn btn-success"
								value="Login"> New User? <a href="registeration.jsp">Register</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>