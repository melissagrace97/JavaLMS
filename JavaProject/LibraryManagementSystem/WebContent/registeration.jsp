<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Register Account</title>
<link rel="stylesheet" href="css/register.css">
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
	
<script type="text/javascript">
	function validate() {
		var name = document.getElementById("name").value;
		var email = document.getElementById("email").value;
		var pass = document.getElementById("pass").value;
		var cpass = document.getElementById("cpass").value;

		if (name == "") {
			document.getElementById("nameerror").innerHTML = "Please enter Name";
			return false;
		} else {
			document.getElementById("nameerror").innerHTML = "";
		}
		if (email == "") {
			document.getElementById("emailerror").innerHTML = "Please enter Email";
			return false;
		} else {
			document.getElementById("emailerror").innerHTML = "";
		}
		if (pass == "") {
			document.getElementById("passerror").innerHTML = "Please enter Password";
			return false;
		} else {
			document.getElementById("passerror").innerHTML = "";
		}
		if (pass.length<5 || pass.length>8) {
			document.getElementById("passerror").innerHTML = "Password should be in between 5 to 8 characters";
			return false;
		} else {
			document.getElementById("passerror").innerHTML = "";
		}
		if (cpass == "") {
			document.getElementById("cpasserror").innerHTML = "Please re-enter Password";
			return false;
		} else {
			document.getElementById("cpasserror").innerHTML = "";
		}
		if (cpass.length<5 || cpass.length>8) {
			document.getElementById("cpasserror").innerHTML = "Password should be in between 5 to 8 characters";
			return false;
		} else {
			document.getElementById("cpasserror").innerHTML = "";

			if (cpass != pass) {
				document.getElementById("cpasserror").innerHTML = "Please enter same password";
				return false;
			} else {
				document.getElementById("cpasserror").innerHTML = "";
			}
		}

		return true;
	}
</script>
</head>
<body>

	<header>
		<nav class="navbar navbar-expand-md navbar-dark"
			style="background-color: black">
			<div>
				<a href="" class="navbar-brand">LIBRARY MANAGEMENT SYSTEM</a>
			</div>
		</nav>
	</header>
	<br>
<div class="row">
		<div class="container">
			<h3 class="text-center">Register</h3>
			<br>
			<p class="status" style="color:red; align:center">
						<%
							String status = (String) request.getAttribute("status");
						if (status != null) {
							out.println(status);
						}
						%>
					<p>
			<div class="container">
				<form onsubmit="return validate()" action="RegisterServlet"
		method="post" class="form-control">
					<div class="form-group">
						<label class="col-sm-3 control-label">Full Name</label>
						<div class="col-sm-6">
							<input type="name" placeholder="Full Name" class="form-control"
								name="name" id="name" required> <span
								style="color: red" id="nameerror">*</span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-sm-3 control-label">Email</label>
						<div class="col-sm-6">
							<input type="email" placeholder="Username" class="form-control"
								name="email" id="email" required> <span
								style="color: red" id="emailerror">*</span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Password</label>
						<div class="col-md-6">
							<input type="password" placeholder="Password" name="password"
								id="pass" required class="form-control"> <span
								style="color: red" id="passerror">*</span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Confirm Password</label>
						<div class="col-md-6">
							<input type="password" placeholder="Confirm Password" name="confirm password"
								id="cpass" required class="form-control"> <span
								style="color: red" id="cpasserror">*</span>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-4 control-label">Select Type</label>
						<div class="col-md-6">
							<select name="role" class="form-control">
								<option value="" selected="selected">- select role -</option>
								<option value="admin">Admin</option>
								<option value="user">User</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<div class="col-md-offset-3 col-md-9 m-t-15">
							<input type="submit" name="btn_register" class="btn btn-success"
								value="Register"> Already have an account? <a href="index.jsp">Login</a>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>