<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Issue Book Request</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src=https://code.jquery.com/jquery-1.12.4.js>
</script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js">
</script>
<script>
  $(function() {
    $("#datepicker").datepicker();
  });
</script>
</head>
<body>
	<header>
		<nav class="navbar navbar-expand-md navbar-dark"
			style="background-color: black">
			<div>
				<a href="" class="navbar-brand">LIBRARY MANAGEMENT SYSTEM</a>
			</div>
			<ul class="navbar-nav">
				<li><a href="user_home.jsp" class="nav-link">Home</a></li>
				<li><a href="user_view_search_books.jsp"
					class="nav-link">Books</a></li>
				<li><a href="user_issue_book_form.jsp" class="nav-link"><strong>Book
							Request</strong></a></li>
				<li><a href="" class="nav-link">My Books</a></li>
				<li><a href="" class="nav-link">Book Renewal</a></li>
				<li><a href="" class="nav-link">Issue Information</a></li>
				<li><a href="logout.jsp" class="nav-link">Logout</a></li>
			</ul>
		</nav>
	</header>
	<br>
	<div class="row">
		<div class="container">
			<h3 class="text-center">Issue Book Form</h3>
			<br>
			<div class="container">
				<form action="IssueBook" method="post">
					<div class="form-group col-md-4">
						<label for="bookid">Book ID</label> <input type="text"
							class="form-control" name="bookid" id="bookid"
							placeholder="Book ID" />
					</div>
					<div class="form-group col-md-4">
						<label for="id">User ID</label> <input type="text"
							class="form-control" name="userid" id="userid"
							placeholder="User ID" />
					</div>
					<div class="form-group col-md-4">
						<label for="issuedate">Issue Date</label> <input type="text"
							class="form-control" name="issuedate" id="datepicker"
							placeholder="Issue Date" />
					</div>
					<button type="submit1" class="btn btn-primary">Request Book</button>
				</form>

			</div>
		</div>
	</div>
</body>
</html>