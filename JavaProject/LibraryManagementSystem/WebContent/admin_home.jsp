<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
</head>
<body>
<header>
		<nav class="navbar navbar-expand-md navbar-dark"
			style="background-color: black">
			<div>
				<a href="" class="navbar-brand">LIBRARY MANAGEMENT SYSTEM</a>
			</div>
			<ul class="navbar-nav">
			<li><a href="admin_home.jsp" class="nav-link"><strong>Home</strong></a></li>
				<li><a href="<%=request.getContextPath()%>/list"
					class="nav-link">Books</a></li>
					<li><a href="search-book.jsp" class="nav-link">Search
						Books</a></li>
				<li><a href="issuebookrequest.jsp" class="nav-link">Issue Book Request</a></li>
				<li><a href="issue_books.jsp" class="nav-link">Issued Books</a></li>
				<li><a href="return_books.jsp" class="nav-link">Return Books</a></li>
				<li><a href="logout.jsp" class="nav-link" >Logout</a></li>
			</ul>
		</nav>
	</header>
	<div class="row">
		<div class="container">
	<br>
			<h1 class="text-center">Welcome Admin!</h1>
		</div>
	</div>
</body>
</html>