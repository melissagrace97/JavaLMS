<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<link rel="stylesheet"
	href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
	crossorigin="anonymous">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<script src=https://code.jquery.com/jquery-1.12.4.js>
</script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js">
</script>
<script>
  $(function() {
    $("#datepicker").datepicker();
    $("#datepicker1").datepicker();
  });
</script>

</head>
<body>
	<header>
		<nav class="navbar navbar-expand-md navbar-dark"
			style="background-color: black">
			<div>
				<a href="" class="navbar-brand">LIBRARY MANAGEMENT SYSTEM</a>
			</div>
			<ul class="navbar-nav">
				<li><a href="admin_home.jsp" class="nav-link">Home</a></li>
				<li><a href="<%=request.getContextPath()%>/list"
					class="nav-link">Books</a></li>
					<li><a href="search-book.jsp" class="nav-link">Search Books</a></li>
				<li><a href="issuebookrequest.jsp"
					class="nav-link"><strong>Issue Book Request</strong></a></li>
					<li><a href="issue_books.jsp" class="nav-link">Issued Books</a></li>
				<li><a href="return_books.jsp" class="nav-link">Return
						Books</a></li>
				<li><a href="logout.jsp" class="nav-link">Logout</a></li>
			</ul>
		</nav>
	</header>
	<br>
	<div class="row">
		<div class="container">
			<br>
			<h3 class="text-center">Issue Books</h3>
			<br>
			<form action="IssueRequest"
				method="post" class="form-control">
				<div class="form-group col-md-4">
					<label for"userid">Reader ID</label>
						<input type="userid" placeholder="Reader ID" class="form-control"
							name="userid" id="uderid" required> 
				</div>
				<div class="form-group col-md-4">
					<label for="bookid">Book ID</label>
						<input type="bookid" placeholder="Book ID" name="bookid"
							id="bookid" required class="form-control">
				</div>
				
					<div class="form-group col-md-4">
						<label for="issuedate">Issue Date</label> <input type="text"
							class="form-control" name="issuedate" id="datepicker"
							placeholder="Issue Date" />
					</div>
						<div class="form-group col-md-4">
						<label for="duedate">Due Date</label> <input type="text"
							class="form-control" name="duedate" id="datepicker1"
							placeholder="Due Date" />
					</div>
				<div class="form-group">
						<div class="col-md-offset-3 col-md-9 m-t-15">
							<input type="submit" name="btn_issue" class="btn btn-primary"
								value="Issue Book">
						</div>
					</div>
			</form>
		</div>
	</div>
</body>
</html>